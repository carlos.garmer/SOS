import { Component, OnInit } from '@angular/core';
import { InfoUsuario } from "../allVARS";
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss'],
})
export class UsuarioPage implements OnInit {

   infoUsuario: any[1]=[1];

  registrado: boolean= false;
  name: string =InfoUsuario.nombre;
  surname: string=InfoUsuario.apellido;
  age: string=InfoUsuario.edad;
  heigth: string=InfoUsuario.estatura;
  weigth: string=InfoUsuario.peso;
  blood: string=InfoUsuario.blood;
  meds: string=InfoUsuario.medicamentos;
  alergy: string=InfoUsuario.alergias;
  donate: string=InfoUsuario.donador;

  constructor(private storage:Storage) {

    this.storage.get('infoUsuario').then(infoUsuario =>{
      if (infoUsuario !=null){
        console.log(infoUsuario)
      this.infoUsuario=infoUsuario;
      this.registrado=true;
      InfoUsuario.nombre= this.infoUsuario[0].name;
      InfoUsuario.apellido= this.infoUsuario[0].surname;
      InfoUsuario.edad= this.infoUsuario[0].age;
      InfoUsuario.estatura= this.infoUsuario[0].heigth;
      InfoUsuario.peso= this.infoUsuario[0].weigth;
      InfoUsuario.blood= this.infoUsuario[0].blood;
      InfoUsuario.donador= this.infoUsuario[0].donador;
      InfoUsuario.medicamentos= this.infoUsuario[0].meds;
      InfoUsuario.alergias= this.infoUsuario[0].alergy;}
      else
      this.infoUsuario= [];
   });
  }

  ngOnInit() {
    
  }

  guardar() {
    this.registrado=true;
    this.infoUsuario[0]={
      name: this.name,
      surname : this.surname,
      age: this.age,
      heigth: this.heigth,
      weigth: this.weigth,
      blood: this.blood,
      donate: this.donate,
      meds: this.meds,
      alergy: this.alergy,
    };
    this.storage.set('infoUsuario',this.infoUsuario);

    InfoUsuario.nombre= this.infoUsuario[0].name;
    InfoUsuario.apellido= this.infoUsuario[0].surname;
    InfoUsuario.edad= this.infoUsuario[0].age;
    InfoUsuario.estatura= this.infoUsuario[0].heigth;
    InfoUsuario.peso= this.infoUsuario[0].weigth;
    InfoUsuario.blood= this.infoUsuario[0].blood;
    InfoUsuario.donador= this.infoUsuario[0].donador;
    InfoUsuario.medicamentos= this.infoUsuario[0].meds;
    InfoUsuario.alergias= this.infoUsuario[0].alergy;
  }

  editar() {
    this.storage.clear();
    this.registrado=false;
  }
  }



