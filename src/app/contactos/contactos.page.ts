import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.page.html',
  styleUrls: ['./contactos.page.scss'],
})

  export class ContactosPage implements OnInit{
    lnames: any[] =[];
    registar=true;
    nombre:string="";
    surname: string;
    tel: string;
    data:string ='';
    iden: any;
    editando: boolean= false;
    panel=true;
  
    constructor(private storage:Storage) {
      this.storage.get('lnames').then(lnames =>{
        if (lnames !=null)
        this.lnames=lnames;
        else
        this.lnames= [];
      });
     
      
    }
  
  
    ngOnInit() {
    }

    registarL(){
      this.registar=false;
    }
    anadirE (){

      let max = this.lnames.length;
          for(var i = 0;i < max;i++){
            if(this.lnames[i].id== this.iden){
              this.lnames[i].name=this.nombre ;
              this.lnames[i].surname=this.surname ;
              this.lnames[i].tel= this.tel;
            }
          }
          this.nombre='';
          this.surname='';
          this.tel='';
          this.editando=false;
          this.storage.set('lnames',this.lnames);
          this.registar=true;
          
    }
    anadir() {

      this.panel=false;
      this.lnames[0]={
        id: this.lnames.length +1,
        name: this.nombre,
        surname : this.surname,
        tel: this.tel,
        birth: this.data
      };
  
  
      this.nombre='';
      this.surname='';
      this.tel='';
      this.storage.set('lnames',this.lnames);
      this.registar=true;
      }
  
      editar(id){
        this.registar=false;
        let max = this.lnames.length;
          for(var i = 0;i < max;i++){
            if(this.lnames[i].id== id){
              this.nombre=this.lnames[i].name ;
              this.surname=this.lnames[i].surname;
              this.tel=this.lnames[i].tel;
            }
          }
          this.editando=true;
          this.iden=id;
      }
  
      eliminar(id){
        this.panel=true;
        let response = []
        for(let item of this.lnames){
          if( item.id!=id){
            response.push(item);
          }
        }
        this.lnames=response;
        this.storage.set('lnames',this.lnames);
      }
    }
