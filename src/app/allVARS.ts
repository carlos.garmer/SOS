/*  Ejemplo de como se debe declarar una clase para su uso global, este ts es como nuestro storage del programa
    export class NombreDeClase{
        static stringInicio = "Studioso";
    }

    Para usar esa clase y/o variable dentro de otro archivo se debe de poner: 
    "import {NombreDeClase} from "directorio""

    Y asi dentro del ts puedes usarlo como una variable normal usando la siguiente sintaxis:
    NombreDeClase.stringInicio;
*/  

export class InfoUsuario {
  static nombre: string ="";
  static apellido: string ="";
  static edad: string ="";
  static blood: string ="";
  static donador: string ="";
  static peso: string ="";
  static estatura: string ="";
  static medicamentos: string ="";
  static alergias: string ="";
}