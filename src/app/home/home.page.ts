import { Component } from '@angular/core';
import { InfoUsuario } from "../allVARS";
import { UsuarioPage } from "../usuario/usuario.page";
import { ContactosPage } from "../contactos/contactos.page";
import { CallNumber } from '@ionic-native/call-number/ngx';
import { SMS } from '@ionic-native/sms/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

name: string =InfoUsuario.nombre;
surname: string=InfoUsuario.apellido;
age: string=InfoUsuario.edad;
heigth: string=InfoUsuario.estatura;
weigth: string=InfoUsuario.peso;
blood: string=InfoUsuario.blood;
meds: string=InfoUsuario.medicamentos;
alergy: string=InfoUsuario.alergias;
donate: string=InfoUsuario.donador;
mens: string="";
long: string="";
lat: string="";
data: string="";
tel1: string="Número no registrado";
verInfoM: boolean=false;
imagen: string ="https://ibb.co/68054Rf";

  constructor(private callNumber: CallNumber, private sms: SMS, private geolocation: Geolocation, private storage:Storage) {

    this.storage.get('infoUsuario').then(infoUsuario =>{
      if (infoUsuario !=null){
        console.log(infoUsuario)
        this.name= infoUsuario[0].name;
        this.surname=infoUsuario[0].apellido;
        this.age=infoUsuario[0].edad;
        this.heigth=infoUsuario[0].estatura;
        this.weigth=infoUsuario[0].peso;
        this.blood=infoUsuario[0].blood;
        this.meds=infoUsuario[0].medicamentos;
        this.alergy=infoUsuario[0].alergias;
        this.donate=infoUsuario[0].donador;}
   });
   this.storage.get('lnames').then(lnames =>{
    if (lnames !=null){
      console.log(lnames)
     if (lnames[0].tel!=null) this.tel1= lnames[0].tel;}
 });

   this.geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    // resp.coords.longitude
    this.long=' Longitud: '+ resp.coords.longitude;
    this.lat= 'Latitud: '+ resp.coords.latitude;
    this.data='Latitud: '+ resp.coords.latitude + ' Longitud: '+ resp.coords.longitude;
   }).catch((error) => {
     console.log('Error getting location', error);
   });   
  }

ubi (){
  this.geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    // resp.coords.longitude
    this.long=' Longitud: '+ resp.coords.longitude;
    this.lat= 'Latitud: '+ resp.coords.latitude;
    this.data='Latitud: '+ resp.coords.latitude + ' Longitud: '+ resp.coords.longitude;
   }).catch((error) => {
     console.log('Error getting location', error);
   });   
}

 ayuda(){
this.callNumber.callNumber("911", true)
.then(res => console.log('Launched dialer!', res))
.catch(err => console.log('Error launching dialer', err));
 }
 mensaje(){
  this.storage.get('lnames').then(lnames =>{
    if (lnames !=null){
      console.log(lnames)
     if (lnames[0].tel!=null) this.tel1= lnames[0].tel;}
 });
  this.geolocation.getCurrentPosition().then((resp) => {
    // resp.coords.latitude
    // resp.coords.longitude
    this.data=' Latitud: '+ resp.coords.latitude +' Longuitud: '+ resp.coords.longitude;
   }).catch((error) => {
     console.log('Error getting location', error);
   });   
  this.mens= this.name +" ha activado el modo de EMERGENCIA desde "+ this.data+ " por la aplicación SAFE";
  this.sms.send(this.tel1, this.mens);
 }
 
}
